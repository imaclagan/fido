let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/assets/sass/app.scss', 'public/css')
    .js('resources/assets/js/main.js', 'public/js')
    .js('resources/assets/js/orderpicker.js', 'public/js').vue({ version: 2 })
    .js('resources/assets/js/call_planner.js', 'public/js').vue({ version: 2 })
    .js('resources/assets/js/stockadjuster.js', 'public/js').vue({ version: 2 })
    .js('resources/assets/js/clientprices.js', 'public/js').vue({ version: 2 })
    .js('resources/assets/js/freightcalc.js', 'public/js').vue({ version: 2 })
    .js(['resources/assets/js/app.js','resources/assets/js/SimpleAjaxUploader.js'],'public/js/all.js')
    .copy('resources/assets/js/imageuploader.js', 'public/js/imageuploader.js')
    .copy('resources/assets/dist','public/dist')
    .version(['css/app.css','js/imageuploader.js','js/all.js','js/main.js','js/call_planner.js','js/orderpicker.js','js/stockadjuster.js','js/clientprices.js','js/freightcalc.js']);

    // elixir(mix => {
    //     mix.sass('app.scss')
    //         .webpack('main.js')
    //         .webpack('orderpicker.js')
    //         .webpack('call_planner.js')
    //         .webpack('stockadjuster.js')
    //         .webpack('clientprices.js')
    //         .webpack('freightcalc.js')
    //         .scripts(['app.js','SimpleAjaxUploader.js'],'public/js/all.js')
    //         .copy('resources/assets/js/imageuploader.js', 'public/js/imageuploader.js')
    //         .copy('resources/assets/dist','public/dist')
    //         .version(['css/app.css','js/imageuploader.js','js/all.js','js/main.js','js/call_planner.js','js/orderpicker.js','js/stockadjuster.js','js/clientprices.js','js/freightcalc.js']);
    // });
    