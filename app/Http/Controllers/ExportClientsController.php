<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Legacy\Client\Client;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class ExportClientsController extends Controller
{
    public function export()
    {
        $client = new Client;
        $dbFields = array_merge(['id'],$client->getFillable(),['client_id','updated_at','created_at']);

        $rows = DB::table('clients')->get();
        // dd($rows);

        //dd(count($rows));

        $data =[];
        $records = 0;

        foreach($rows as $row) {
            $records++;

            foreach($dbFields as $field){
                if(property_exists($row,$field)){
                    $data[$records][$field] = $row->$field;
                } else {
                    // do not know how this could happen??
                    continue;
                }
                
            }
            

        }

        //dd($data);
        
        $type='xls';

        return Excel::create('client_table_export_'.date('dmY_His'), function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }
    
}
