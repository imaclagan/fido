<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Legacy\Client\Client;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;


class ImportClientsController extends Controller
{


    public function index()
    {
        return view('admin.import.clients');
    }

    public function import()
    {
        // dd('importing file');

        $inserts = 0;
        $updates = 0;
        $skipped = 0;
        $n = 1; // spreadsheet row counter
        $skippedClients =[];



        if (Input::hasFile('spreadsheet')) {

            $path = Input::file('spreadsheet')->getRealPath();
            $data = Excel::load($path, function ($reader) {
            })->get();

            //dd($data);
            if (!empty($data) && $data->count()) {

                foreach ($data as $key => $row) {
                    $n++;
                    

                    // Check if this import has an ID field, if yes then update, if no then insert
                    // dd($row);
                    if ($row->has('name') && $row->has('client_id') && $row->client_id > 0) {
                        // its an UPDATE
                        if (!$client = Client::find($row->client_id)) {
                            // skip this one because id cant be found
                            $skipped++;
                            echo 'Skipping row ' . $n . ', cannot find client with ID = '. $row->client_id . '<br>';
                            continue;
                        }
                        $updates++;
                        echo 'Updating ID = '.$row->client_id .' - name = '.$row->name .'<br>';

                        foreach ($row->keys() as $key) {
                            // echo $key .'::'.$row->$key . '<br>';
                             if ($client->hasField($key)) {
                                 //echo 'haskey ';
                                 $client->$key = $row->$key;
                             } 
                         };

                         $client->save();

                    }  else if($row->has('email_address') && ! empty($row->email_address)) {
                        // its an INSERT from a mailchimp export
                        
                        // Check if email already exists in am client record
                        $clientCheck = $this->getClientIdByEmail($row->email_address);
                        if($clientCheck){
                            $skipped++;
                            echo 'Skipping row ' . $n . ' because email ' . $row->email_address . ' is already used by ' .$clientCheck->name .'('.$clientCheck->client_id.')<br>';
                            $skippedClients[$skipped]= $row->email_address . ' is already used by ' .$clientCheck->name .' Client_ID:'.$clientCheck->client_id;
                            continue;
                        }
                        

                        $inserts++;
                        echo 'Inserting client with email = '.$row->email_address.'<br>';

                        $client = new Client;

                        // Set some sensible defaults
                        $client->status = 'active';
                        $client->salesrep = 99;
                        $client->myob_record_id =99;
                        $client->call_frequency = 9999;
                        $client->call_planning_note = 'mailchimp import';

                        foreach ($row->keys() as $key) {
                            // echo $key .'::'.$row->$key . '<br>';
                             if ($client->hasField($key)) {
                                 //echo 'haskey ';
                                 $client->$key = $row->$key;
                             }
                             
                             if ($key =='email_address') {
                                 $client->email_1 = $row->$key;
                             }

                             if($key == 'company_name') {

                                if(!empty($row->key)){
                                    $client->name = $row->key;
                                } else {
                                    $client->name = $row->email_address;
                                    $client->save(); // need to save to get client_id
                                    $client->name = 'Client ' . $client->client_id;
                                }
                                
                             }

                             if($key == 'phone_number') {
                                $client->phone = $row->key;
                             }


                         };

                        
                        $client->save();

                        //dd($client);
                    } else {
                        $skipped++;
                        echo 'Skipping row ' . $n .' because no email address found in row';
                        continue;
                        
                    }

                    // dd($row);

                    // assign row values to client object
                    // if row does not have a name we need to create one
                    if( empty($row->name) ) {
                        $row->name = $row->email;
                    }

                    foreach ($row->keys() as $key) {
                       // echo $key .'::'.$row->$key . '<br>';
                        if ($client->hasField($key)) {
                            //echo 'haskey ';
                            $client->$key = $row->$key;
                        } elseif ($key =='email') {
                            $client->email_1 = $row->$key;
                        }
                    };

                }
            }


            echo '<hr>';
            echo $inserts. ' Records inserted <br>';
            echo $updates. ' Records updated <br>';
            echo $skipped. ' Rows skipped <br>';
            foreach($skippedClients as $r){
                echo $r.'<br>';
            }
            echo 'All done.<hr>';

            // dd('done import');
            //dd(collect($results[1276]));

        }
    }

    public function getClientIdByEmail($email)
    {
        $client = Client::where('email_1',$email)
                ->orWhere('email_2',$email)
                ->orWhere('email_3',$email)
                ->first();
        
        if ($client->client_id > 0){
            // echo 'CLIENT FOUND';
            //dd([$email,$client]);
            return $client;
        }

        return false;

        
    }
}
