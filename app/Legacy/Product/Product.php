<?php

namespace App\Legacy\Product;

use Carbon\Carbon;
use App\Legacy\Order\Item;
use App\Events\ProductSaved;
use App\Legacy\Traits\UsersQueryFilter;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use UsersQueryFilter;
    

    /**
     * The connection name for the model.
     *
     * @var string
     */
    // protected $connection = 'k9homes';

    /**
     * The table primary key column.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    public $timestamps = false;

    protected $fillable = [
        'description',
        'size',
        'price',
        'product_code',
        'typeid',
        'qty_break',
        'qty_discount',
        'qty_instock',
        'qty_ordered',
        'special',
        'clearance',
        'can_backorder',
        'status',
        'modified',
        'cost',
        'last_costed_date',
        'supplier',
        'width',
        'height',
        'length',
        'shipping_volume',
        'shipping_weight',
        'actual_weight',
        'shipping_container',
        'display_order',
        'barcode',
        'color_name',
        'color_background_color',
        'color_background_image',
        'notify_when_instock',
        'source',
        'new_product',
        'core_product',
        'low_stock_level',
        'rrp',
        'product_note',
        'shopify_id',
        'shopify_published',
        'shopify_handle',
        'shopify_option1_name',
        'shopify_option1_value',
        'shopify_option2_name',
        'shopify_option2_value',
        'shopify_option3_name',
        'shopify_option3_value',
        'shopify_image_src'
    ];

    static function boot()
    {
        Product::saved(function ($product) {

            
            // Update any client prices for the product
            $product->clientPrices->each(function($clientPrice,$key) use($product) {
                if($product->status !== 'inactive' && ($product->price != $clientPrice->std_price)){
                    // now add the std_price and the calculated client_price and then save
                    $clientPrice->std_price = $product->price;
                    $clientPrice->client_price = $product->price * (1 - $clientPrice->discount);
                    
                    $clientPrice->save();
                    
                }
            });
            
        });

        
    }

    public function hasField($name=null)
    {
        return in_array($name,$this->fillable);
    }

    public function recentSales($period = -1)
    {
        
        

        $sales = $this->sales($period)->get();

        // sum up the qty * price

        $totalSales = $sales->reduce(function ($totalSales, $item) {

            return $totalSales + ($item->qty * $item->price);
        });

        $totalUnits = $sales->reduce(function ($totalUnits, $item) {

            return $totalUnits + $item->qty;
        });


        return (object) ['sales'=>$totalSales,'units'=>$totalUnits];
    }


    // Returns the barcode with check digit
    function checkdigit( $digits)
	{
		if(!$digits){
			return '';
		}	
		
		$digits = (string) $digits;
		
		if(strlen($digits) != 12){
			return '';
		}
		
		// 1. Add the values of the digits in the even-numbered positions: 2, 4, 6, etc.
		$even_sum = $digits[1] + $digits[3] + $digits[5] + $digits[7] + $digits[9] + $digits[11];
		// 2. Multiply this result by 3.
		$even_sum_three = $even_sum * 3;
		// 3. Add the values of the digits in the odd-numbered positions: 1, 3, 5, etc.
		$odd_sum = $digits[0] + $digits[2] + $digits[4] + $digits[6] + $digits[8] + $digits[10];
		// 4. Sum the results of steps 2 and 3.
		$total_sum = $even_sum_three + $odd_sum;
		// 5. The check character is the smallest number which, when added to the result in step 4,  produces a multiple of 10.
		$next_ten = (ceil($total_sum / 10)) * 10;
		$check_digit = $next_ten - $total_sum;

		return $check_digit;
	}

    /**
     * Scope a query to only include filtered results.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeApplyUserFilter($query)
    {
       return $this->applyFilter($query, $this->table);

    }

    public function bom()
    {
        return $this->hasMany(Bom::class, 'parent_product_code', 'product_code');
    }


    public function sales($period = false)
    {
        
        $query =  $this->hasMany(Item::class,'product_code','product_code');

        if($period > -1) {
            $query->where('updated_at', '>' , Carbon::now()->subDays($period));
        }

        return $query;
    }

    public function clientprices()
    {

        return $this->hasMany(ClientPrice::class, 'product_code', 'product_code');
        //->select(['product_code', 'client_id']);

    }

}
