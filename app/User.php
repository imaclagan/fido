<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Legacy\Staff\User as Staff;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];
    protected $privileges; //Provided by ecatalog user profile

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function hasRole($role)
    {

        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        return !!$this->roles->intersect($this->roles)->count();
    }

    public function hasPrivilege($privilege = '')
    {
        
        // Load ecat user profile to get access to privileges
        $staff = Staff::where('user_id',$this->id)->first();

        // Test to see if privelege exists in the list of csv privileges
        return str_contains($staff->privileges,$privilege);
    }
}
