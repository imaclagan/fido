<?php
use App\Legacy\Product\Product;

class AuthTest extends TestCase
{
    /** @test t*/
    public function the_catalog_can_be_accessed()
    {
        $response = $this->call('GET', '/catalog');
        $this->assertEquals(200, $response->status());

        $this->assertSee('fred');
    }
}