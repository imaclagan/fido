/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmory imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmory exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		Object.defineProperty(exports, name, {
/******/ 			configurable: false,
/******/ 			enumerable: true,
/******/ 			get: getter
/******/ 		});
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

eval("\n\nvar app =new Vue({\n    el: '#table',\n    data: {\n        columns: [\n            { label: 'ID', field: 'id', align: 'center', filterable: false },\n            { label: 'Username', field: 'user.username' },\n            { label: 'First Name', field: 'user.first_name' },\n            { label: 'Last Name', field: 'user.last_name' },\n            { label: 'Email', field: 'user.email', align: 'right', sortable: false }\n        ],\n        rows: window.rows\n    }\n});//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiMC5qcyIsInNvdXJjZXMiOlsid2VicGFjazovLy9yZXNvdXJjZXMvYXNzZXRzL2pzL2NhbGxfcGxhbm5lci5qcz83NDkwIl0sInNvdXJjZXNDb250ZW50IjpbIlxuXG5jb25zdCBhcHAgPW5ldyBWdWUoe1xuICAgIGVsOiAnI3RhYmxlJyxcbiAgICBkYXRhOiB7XG4gICAgICAgIGNvbHVtbnM6IFtcbiAgICAgICAgICAgIHsgbGFiZWw6ICdJRCcsIGZpZWxkOiAnaWQnLCBhbGlnbjogJ2NlbnRlcicsIGZpbHRlcmFibGU6IGZhbHNlIH0sXG4gICAgICAgICAgICB7IGxhYmVsOiAnVXNlcm5hbWUnLCBmaWVsZDogJ3VzZXIudXNlcm5hbWUnIH0sXG4gICAgICAgICAgICB7IGxhYmVsOiAnRmlyc3QgTmFtZScsIGZpZWxkOiAndXNlci5maXJzdF9uYW1lJyB9LFxuICAgICAgICAgICAgeyBsYWJlbDogJ0xhc3QgTmFtZScsIGZpZWxkOiAndXNlci5sYXN0X25hbWUnIH0sXG4gICAgICAgICAgICB7IGxhYmVsOiAnRW1haWwnLCBmaWVsZDogJ3VzZXIuZW1haWwnLCBhbGlnbjogJ3JpZ2h0Jywgc29ydGFibGU6IGZhbHNlIH1cbiAgICAgICAgXSxcbiAgICAgICAgcm93czogd2luZG93LnJvd3NcbiAgICB9XG59KTtcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gcmVzb3VyY2VzL2Fzc2V0cy9qcy9jYWxsX3BsYW5uZXIuanMiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsInNvdXJjZVJvb3QiOiIifQ==");

/***/ }
/******/ ]);