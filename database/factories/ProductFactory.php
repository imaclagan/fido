<?php

use App\Legacy\Product\Product;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Product::class, function (Faker\Generator $faker) {
    return [
        'description' => $faker->sentence,
        'size' => $faker->word,
        'price' => $faker->randomNumber(5),
        'product_code' => $faker->name,
        'typeid' => $faker->randomNumber(6),
        'qty_break' => $faker->numberBetween(0,20),
        'qty_discount' => $faker->numberBetween(0,20),
        'qty_instock' => $faker->randomNumber(2),
        'qty_ordered' => $faker->numberBetween(0,20),
        'special' => 0,
        'clearance' => 0,
        'can_backorder' => 'n',
        'status' => 'active',
        'modified' => $faker->date(),
        'cost' => $faker->randomNumber(3),
        'last_costed_date' => $faker->date(),
        'supplier' => $faker->name,
        'width' => 1,
        'height' => 1,
        'length' => 1,
        'shipping_volume' => 1,
        'shipping_weight' => 1,
        'actual_weight' => 2,
        'shipping_container' => 'small',
        'display_order' => $faker->numberBetween(0,10),
        'barcode' => $faker->ean13(),
        'color_name' => 'blue',
        'color_background_color' => "#ffffff",
        'color_background_image' => "",
        'notify_when_instock' => "y",
        'source' => "a",
        'new_product' => 0,
        'core_product' => 1,
        'low_stock_level' => 10,
        'rrp' => $faker->randomNumber(3),
        'product_note' => $faker->sentence,
        
    ];
});
